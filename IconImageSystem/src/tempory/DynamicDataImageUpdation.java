package tempory;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class DynamicDataImageUpdation {

	private static String from_id;
	private static String from_field;

	private static String host = "178.32.49.5";
	private static int port =27017;
	private static String inDB = "crawler";
	private static String collfrom = "imagedata";
	private static String collto = "sampleCrawldata";
	private static String id = "domainLink";
	private static String chngfield = "image";
	@SuppressWarnings({ "resource", "deprecation" })
	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient( host , port );
		DB db = mongoClient.getDB(inDB);
		DBCollection imagedata = db.getCollection(collfrom);
		DBCollection sampleCrawldata = db.getCollection(collto);

		//find data from imagedata
		DBCursor cursor = imagedata.find();
		int count = 1;
		while (cursor.hasNext()) {
			DBObject images = cursor.next();
			from_id = images.get(id).toString();
			from_field = images.get(chngfield).toString();
			//get imagedata.domainLink and imagedata.image
			System.out.println((count++)+"# updating for domainlink: "+from_id);
			//////////////////////////////////////////////////////////

			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.append("domainLink", from_id);
			BasicDBObject updateQuery = new BasicDBObject();
			updateQuery.append("$set",new BasicDBObject().append("image", from_field));
			sampleCrawldata.updateMulti(searchQuery, updateQuery);

		}
	}
}
