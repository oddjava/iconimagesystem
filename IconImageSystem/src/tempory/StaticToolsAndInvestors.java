package tempory;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class StaticToolsAndInvestors {

	public static void main(String[] args) 
	{
		MongoClient mongoClient = new MongoClient( "178.32.49.5" , 27017 );
		DB db = mongoClient.getDB("crawler");
		
		DBCursor cursor;
		
		DBCollection imagedata = db.getCollection("crawldata12-7-16");
		
		DBCollection sampleCrawldata = db.getCollection("sampleCrawldata");
		DBCollection crawldata = db.getCollection("crawldata");
				
		List<BasicDBObject> coll = new ArrayList<BasicDBObject>();
		coll.add(new BasicDBObject("subCategory", "Tools")); 
		coll.add(new BasicDBObject("subCategory", "Investors"));
		
		cursor=imagedata.find(new BasicDBObject("$or", coll));
		
		DBObject present;
		String image,link;
		while(cursor.hasNext())
		{
			image="no.jpg";
			link="";
			
			present=cursor.next();
			
			link=present.get("link").toString();
			image=present.get("image").toString();
			
			if(!image.equals("no.jpg"))
			{
				sampleCrawldata.update(new BasicDBObject("link",link), new BasicDBObject("$set",new BasicDBObject("image",image)));
				crawldata.update(new BasicDBObject("link",link), new BasicDBObject("$set",new BasicDBObject("image",image)));
			}	
		}
		

	}

}
