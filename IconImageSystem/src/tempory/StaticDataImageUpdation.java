package tempory;



import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.apache.commons.io.FilenameUtils;
import org.jsoup.Connection.Response;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import org.jsoup.Jsoup;

public class StaticDataImageUpdation {

	public static void main(String[] args) throws IOException 
	{		
		MongoClient client;
		DB crawler;
		DBCollection sampleCrawldata;
		DBCollection crawldata;
		
		
		DBCursor cursor;
		
		client= new MongoClient("178.32.49.5",27017);
			
		crawler=client.getDB("crawler");
				
		sampleCrawldata=crawler.getCollection("sampleCrawldata");
		crawldata=crawler.getCollection("crawldata");
		
		List<BasicDBObject> coll = new ArrayList<BasicDBObject>();
		coll.add(new BasicDBObject("subCategory", "Tools")); 
		coll.add(new BasicDBObject("subCategory", "Investors"));
		
		cursor=crawldata.find(new BasicDBObject("$or", coll).append("image", "no.jpg"));
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		DBObject present;
		String filename; 
		int count=0;
		System.out.println(cursor.count());
		while(cursor.hasNext())
		{
			filename="";
			System.out.println(++count);
			present=cursor.next();
			filename=mydownloader(present.get("domainLink").toString());
			crawldata.update(present, new BasicDBObject("$set",new BasicDBObject("image",filename)));
			sampleCrawldata.update(present, new BasicDBObject("$set",new BasicDBObject("image",filename)));
		}	
		
	}
	private static String mydownloader(String link) 
	{
		String filename =link.replace(".", "_").replace("/", "_")+".";
		String iconlink = null;
		try 
		{
			iconlink = "http://localhost:8000/icon?size=48..120..500&url="+URLEncoder.encode(link, "UTF-8");
		} 
		catch (UnsupportedEncodingException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return "no.jpg";
		}
		//Open a URL Stream
		Response resultImageResponse;
		try
		{
			resultImageResponse= Jsoup.connect(iconlink)
				.followRedirects(true)
				.userAgent("firefox")
				.timeout(0)
				.ignoreContentType(true)
				.execute();
		}
		catch(Exception e)
		{
			return "no.jpg";
		}
		
		String name = null;
		try 
		{
			name = filename+FilenameUtils.getExtension(new URL(resultImageResponse.url().toString()).getPath());
		}
		catch (MalformedURLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "no.jpg";
		}		
		System.out.println(name);
		//output here
		FileOutputStream out = null;
		try
		{
			out = (new FileOutputStream(new java.io.File("./images/"+name)));
		}
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "no.jpg";
		}
		try 
		{
			out.write(resultImageResponse.bodyAsBytes());
			out.close();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "no.jpg";
		}  // resultImageResponse.body() is where the image's contents are.
		
		return name;
	}
}
